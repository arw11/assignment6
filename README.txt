**mersenne.h and mersenne.cpp were sourced from  http://create.stephan-brumme.com/mersenne-twister/ [Accessed 27th November 2015] **

For question 1:
	 type " g++ q1.cpp mersenne.cpp -o q1" to compile
	 type "./q1" to run
	 for quick convergence choose 0.001 accuracy
	 vector size determines dimensionality; written in the code is a 2-D function x*y therefore please select '2'	 
	 
For question 2:
	 
	 Part i):
	 type " g++ q2i.cpp mersenne.cpp -o q2i" to compile
	 type "./q2i" to run
	 choose 0.000001 accuracy
	 choose vector size to be 1
	 choose lower limit to be 0 and upper limit to be 2
	 (calculation takes ~1 hour)
	 
	 Part ii):
	 type " g++ q2ii.cpp mersenne.cpp -o q2ii" to compile
	 type "./q2ii" to run
	 choose 0.000001 accuracy
	 choose vector size to be 1
	 choose lower limit to be 0 and upper limit to be 2
	 (calculation takes ~30 mins)