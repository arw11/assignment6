/*This program uses MonteCarlo with importance sampling to evaluate erf(2)  */
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>
/*mersenne.h and mersenne.cpp were sourced from
  http://create.stephan-brumme.come/mersenne-twister/ */
#include "mersenne.h"

using namespace std;

MersenneTwister prng(20); //Change seed for random number generator here

double f(vector<double> x)
{
	const double pi=acos(-1.0);
	return (2.0/sqrt(pi))*exp(-x[0]*x[0]) ; //Modify function to be integrated here
}	

double w()
{
	const double A=-0.48 , B=0.98;
	const double MAX = 4294967295.0;
	double R = prng()/MAX;
	return -sqrt((2/A)*(R+((B*B)/(2*A))))-(B/A) ; //Linear distribution cdf inverse
}	
	
int main()
{
	double eps; //eps is the relative accuracy required
	cout << "Please enter your relative accuracy: ";
	cin >> eps;

	long int M;

	const double A=-0.48 , B=0.98;
	const double MAX = 4294967295.0;
	double I1,I2;
	
	int N;
	cout << "Choose size of vector: ";
	cin >> N;

	vector<double> x(N);
	vector<double> lowlim(N), uplim(N);

// Program asks user for limits of integration
	for(int v=0; v<N ; v++)
	{
		cout<< "Enter lower limit for direction number " << v << " : ";
		cin >> lowlim[v];
		cout<< "Enter upper limit for direction number " << v << " : ";
		cin >> uplim[v];
	}

	I1 = 0.0; I2 = 0.0;	

//First approximation to the integral
	for( int i= 0 ; i < 100; i++)
	{
		for(int I=0;I<N;I++)
		{
			x[I] = w();
		}

		I1 = I1 + (f(x)/((A*x[0])+B));
	}

	I1 = I1/100.0;

//Second approximation to the integral
	for( int j= 0 ; j < 200; j++)
	{
		for(int J=0;J<N;J++)
                {
                        x[J] = w();
                } 
		I2 = I2 + (f(x)/((A*x[0])+B));
	}
	
	I2 = I2/200.0;	

	M = 200;

	cout << "Integral   No. of steps" << endl;

//Compares previous two approximations and performs a loop which doubles the number of samples taken in each iteration

	while( abs((I2-I1)/I1) > eps)
	{	
		I1 = 0.5*(I1+I2);
		I2 = 0.0;
		M=2*M;
		for(long int k=0; k<M; k++)
		{
		for(int K=0;K<N;K++)
         	       {
                	        x[K] = w();
              		} 
		I2 = I2 +(f(x)/((A*x[0])+B));
		}
		I2 = I2/double(M);
		cout << I2 << "   " << M << endl;
	}

	I2 = 0.5*(I1+I2);

	cout << "The integral is: " << I2 << endl;
	cout << "Number of samples taken is: " << M << endl;
	return 0;
}	
