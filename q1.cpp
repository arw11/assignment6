/* This program calculates the multidimensional integral for a given function between user-specified limits  */
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <vector>
/*mersenne.h and mersenne.cpp were sourced from
  http://create.stephan-brumme.come/mersenne-twister/ */
#include "mersenne.h"

using namespace std;

MersenneTwister prng(30); //Change seed here

double f(vector<double> x) //Change the function being integrated here
{
	return x[0]*x[1];
}	

double w()
{
	const double MAX = 4294967295.0;
	double R = prng()/MAX;
	return R; //alter to change cdf function of R
}	
	
int main()
{
	double eps; //eps is the relative accuracy required
	cout << "Please enter your relative accuracy: ";
	cin >> eps;

	long int M;

	double I1,I2;
	
	int N;
	cout << "Choose size of vector: ";
	cin >> N;

	vector<double> x(N);
	vector<double> lowlim(N), uplim(N);

//User inputs limits for the function being integrated here
	for(int v=0; v<N ; v++)
	{
		cout<< "Enter lower limit for direction number " << v+1 << " : ";
		cin >> lowlim[v];
		cout<< "Enter upper limit for direction number " << v+1 << " : ";
		cin >> uplim[v];
	}

	double hcube = 1.0;


//Creates hypercube for a given multidimensional function
	for(int h=0; h<N ; h++)
	{
		hcube = hcube * (uplim[h]-lowlim[h]);
	}

	I1 = 0.0; I2 = 0.0;	

//Creates first approximation for integral
	for( int i= 0 ; i < 100; i++)
	{
		for(int I=0;I<N;I++)
		{
			x[I] =lowlim[I]+ ((uplim[I]-lowlim[I])*w());
		}

		I1 = I1 + (f(x)*hcube);
	}

	I1 = I1/100.0;

//Creates second approximation for integral
	for( int j= 0 ; j < 200; j++)
	{
		for(int J=0;J<N;J++)
                {
                        x[J] =lowlim[J] + ((uplim[J]-lowlim[J])*w());
                } 
		I2 = I2 + (f(x)*hcube);
	}
	
	I2 = I2/200.0;	

	M = 200;

//Loop checks the convergence condition until it's satisfied
	while( abs((I2-I1)/I1) > eps)
	{	
		I1 = 0.5*(I1+I2); //Change I1 to average of I1 and I2
		I2 = 0.0; //Reset I2 to begin a new independent summation approximation
		M=2*M;
		for(long int k=0; k<M; k++)
		{
		for(int K=0;K<N;K++)
         	       {
                	        x[K] =lowlim[K] +((uplim[K]-lowlim[K])*w());
              		  } 
		I2 = I2 +(f(x)*hcube);
		}
		I2 = I2/double(M);
		cout << I2 << endl; //Allows user to check progress of the Monte Carlo integration method
	}

	I2 = 0.5*(I1+I2);

	cout << "The integral is: " << I2 << endl;
	cout << "Number of samples taken is: " << M << endl;
	return 0;
}	
